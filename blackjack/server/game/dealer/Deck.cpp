#pragma once
#include <algorithm>
#include <ctime>
#include "Deck.h"


namespace deck {

	static std::unordered_map<Rank, RankInfo> RankInfoMap = {
		{ Rank::ACE,   { "ace",   11 } },
		{ Rank::KING,  { "king",  10 } },
		{ Rank::QUEEN, { "queen", 10 } },
		{ Rank::JACK,  { "jack",  10 } },
		{ Rank::TEN,   { "ten",   10 } },
		{ Rank::NINE,  { "nine",  9 } },
		{ Rank::EIGHT, { "eight", 8 } },
		{ Rank::SEVEN, { "seven", 7 } },
		{ Rank::SIX,   { "six",   6 } },
		{ Rank::FIVE,  { "five",  5 } },
		{ Rank::FOUR,  { "four",  4 } },
		{ Rank::THREE, { "three", 3 } },
		{ Rank::TWO,   { "two",   2 } }
	};

	static std::unordered_map<Suit, std::string> SuitNames = {
		{ Suit::HEART,   "heart" },
		{ Suit::DIAMOND, "diamond" },
		{ Suit::CLUB,    "club" },
		{ Suit::SPADE,   "spade" }
	};



	// Card
	Card::Card(Suit suit, Rank rank) 
		: suit(suit)
		, rank(rank) {
	}


	int Card::getPoints() const {
		return RankInfoMap[rank].points;
	}


	std::string Card::getName() const {
		std::string name;
		name += RankInfoMap[rank].name;
		name += " ";
		name += SuitNames[suit];
		return name;
	}



	// Hand
	int Hand::getScore() const {
		int totalScore = 0;
		int aceCount = 0;

		size_t size = cards.size();
		for (size_t i = 0; i < size; ++i) {

			// Aces aren't summarized here
			if (cards[i].rank == Rank::ACE) {
				aceCount++;
				continue;
			}

			totalScore += cards[i].getPoints();
		}

		// Ace cost - 1 or 11 (11, if the total cost isn't more than 21, otherwise 1)
		int aceScore = aceCount * RankInfoMap[Rank::ACE].points;
		if ((totalScore + aceScore) <= 21) {
			totalScore += aceScore;
		}
		else {
			totalScore += aceCount;
		}

		return totalScore;
	}


	std::string Hand::getScoreString() const {
		std::string result;
		result += "score: ";
		result += std::to_string(getScore());

		if (isBlackjack()) {
			result += " BLACKJACK";
		} 
		else if (isBust()) {
			result += " BUST";
		}

		return result;
	}


	void Hand::add(Card card) {
		cards.emplace_back(card);
	}


	void Hand::drop() {
		cards.clear();
	}


	bool Hand::isBlackjack() const {
		return cards.size() == 2 && getScore() == 21;
	}


	bool Hand::isTwentyOne() const {
		return cards.size() > 2 && getScore() == 21;
	}


	bool Hand::isBust() const {
		return getScore() > 21;
	}


	std::string Hand::toString() const {
		std::string result;

		for (auto c : cards) {
			result += c.getName();
			result += "\n";
		}

		result += getScoreString();
		return result;
	}



	// Deck
	Deck::Deck() {
		for (int rank = (int)Rank::ACE; rank <= (int)Rank::TWO; ++rank) {
			for (int suit = (int)Suit::HEART; suit <= (int)Suit::SPADE; ++suit) {

				cards_.emplace_back(
					static_cast<Suit>(suit),
					static_cast<Rank>(rank)
				);

				order_.push_back(count_++);
			}
		}
	}
	
	int myrandom(int i) {
		return std::rand() % i; 
	}

	void Deck::shuffle(bool reset) {
		if (reset) {
			order_.clear();
			for (int i = 0; i < count_; ++i) {
				order_.push_back(i);
			}
		}

		std::srand(unsigned(std::time(0)));
		std::random_shuffle(order_.begin(), order_.end(), myrandom);
	}


	Card Deck::pop() {
		if (order_.empty() && isShuffleMachine_) {
			shuffle();
		}

		int index = order_.front();
		order_.pop_front();

		return cards_[index];
	}
}
