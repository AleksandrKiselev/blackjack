#pragma once
#include <string>
#include <memory>

namespace service {

	const int DefaultPort = 8083;
	using ConnectionId = std::string;

	using OnConnectCallback = std::function<void(ConnectionId)>;
	using OnCloseCallback = std::function<void(ConnectionId)>;
	using OnReadCallback = std::function<void(ConnectionId, std::string)>;
	using OnThrowCallback = std::function<void(std::string)>;


	// base server connection class
	class ServerConnection {
	protected:
		OnConnectCallback onConnect_;
		OnCloseCallback onClose_;
		OnReadCallback onRead_;
		OnThrowCallback onThrow_;


	public:
		virtual ~ServerConnection() = default;

		virtual bool initialize() = 0;
		virtual bool handleConnectionActivity() = 0;
		virtual bool write(ConnectionId to, std::string text) = 0;
		virtual void broadcast(ConnectionId except, std::string text) = 0;
		virtual void disconnect(ConnectionId user) = 0;

		void setOnConnect(OnConnectCallback onConnect) { this->onConnect_ = onConnect; }
		void setOnClose(OnCloseCallback onClose) { this->onClose_ = onClose; }
		void setOnRead(OnReadCallback onRead) { this->onRead_ = onRead; }
		void setOnThrow(OnThrowCallback onThrow) { this->onThrow_ = onThrow; }
	};

	using ServerConnectionPtr = std::shared_ptr<ServerConnection>;

}
