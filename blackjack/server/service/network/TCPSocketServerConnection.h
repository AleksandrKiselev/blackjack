#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment (lib, "ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>

#include <unordered_map>
#include <functional>
#include <iostream>                                                                                                              
#include <vector>   
#include <string>     
#include <set>   

#include "ServerConnection.h"

namespace service {


	// non-blocking tcp winsock connection
	class TCPSocketServerConnection : public ServerConnection {
	private:
		std::unordered_map<SOCKET, ConnectionId> connections_;
		std::set<SOCKET> slaveSockets_;
		SOCKET masterSocket_;
		int port_;


	protected:
		void close(SOCKET sock, bool shdwn = true);
		SOCKET find(ConnectionId id);


	public:
		TCPSocketServerConnection();
		TCPSocketServerConnection(int port);
		virtual ~TCPSocketServerConnection();

		virtual bool initialize() override;
		virtual bool handleConnectionActivity() override;

		virtual bool write(ConnectionId to, std::string text) override;
		virtual void broadcast(ConnectionId except, std::string text) override;

		virtual void disconnect(ConnectionId user) override;
	};

}
