#pragma once

namespace service {

	class NonCopyable {
	public:
		NonCopyable() = default;

	private:
		// copy and assignment not allowed
		NonCopyable(const NonCopyable&) = delete;
		NonCopyable& operator=(const NonCopyable&) = delete;
	};

}