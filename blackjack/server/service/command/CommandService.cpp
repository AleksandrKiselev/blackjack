#include "CommandService.h"
#include "Command.h"


namespace service {

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// CommandService
	CommandService::CommandService(int workersCount) 
		: workersCount_(workersCount) {
	}


	bool CommandService::initialize() {
		auto sharedThis = downcasted_shared_from_this<CommandService>();
		for (int i = 0; i < workersCount_; ++i) {
			workers_.emplace_back(new CommandWorker(sharedThis));
		}
		return true;
	}



	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// ComandWorker
	CommandWorker::CommandWorker(CommandServicePtr service) 
		: service_(service) { 
	}


	std::string CommandWorker::getName() {
		return "CommandWorker"; 
	}


	bool CommandWorker::doWork() {
		if (CommandServicePtr servLock = service_.lock()) {
			Address from = servLock->getAddress();

			if (CommandPtr command = CommandSystem::receive(from)) {
				if (CommandSystem::getTraceCommand()) {
					servLock->getLogger() << "exec " + command->toString();
				}

				command->execute(servLock);
			}
			return true;
		}

		return false;
	}

}
