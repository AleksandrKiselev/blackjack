#pragma once
#include <mutex>
#include "Deck.h"
#include "Player.h"
#include "DealerFeedbackProcessor.h"

#include "../GameHeader.h"
#include "../../service/command/CommandService.h"
#include "../../service/network/ServerConnection.h"


// DealerService
class DealerService : public CommandService {
public:
	static const std::string DEALER_SERVICE_NAME;

private:
	PlayerStateHandlers playerStateHandlers_;
	PlayerCommands playerCommands_;
	PlayersMap players_;

	DealerFeedbackProcessor feedback_;
	std::atomic_bool isGameStarted_;
	std::recursive_mutex mutex_;

	int maxThinkTimeMsec_;
	int sleepTimeMsec_;	

	PlayerPtr dealer_;
	deck::Deck deck_;

	
protected:
	virtual bool initialize() override;
	void initPlayerStateHandlers();
	void initPlayerCommands();

	void setPlayerCommand(std::string name, PlayerCommand command);
	void setPlayerStateHandler(PlayerState state, PlayerStateHandler handler);


	// game phases
	bool startGamePhase();
	bool placeBetsPhase();
	bool playersGamePhase();
	bool dealerGamePhase();
	bool endGamePhase();


	// players functions
	PlayerPtr getPlayerByName(std::string name);
	PlayersVec getPlayersByState(PlayerState state);
	int getPlayersByStateCount(PlayerState state);

	void setState(PlayerState state);
	void setState(PlayerPtr player, PlayerState state);
	void setState(PlayerPtr player, PlayerState state, std::function<bool(void)> setIf);
	void setState(PlayerState state, std::function<bool(PlayerPtr)> setIf);

	void changeState(PlayerState oldst, PlayerState newst);
	void changeState(PlayerPtr player, PlayerState oldst, PlayerState newst);

	void handlePlayerState(PlayerPtr player);
	void handlePlayersState(PlayersVec players);
	

	// card functions
	bool isTimeOver(PlayerPtr state);
	void takeCard(PlayerPtr player, int count = 1);
	void dropHand(PlayerPtr player);

	bool checkHand(PlayerPtr player);
	bool checkBlackjack(PlayerPtr player);
	bool checkTwentyOne(PlayerPtr player);
	bool checkBust(PlayerPtr player);


public:
	DealerService(int workersCount = 1);
	virtual std::string getName() override;


public:
	// dealer service functionality
	bool handleGameLoop();
	bool addPlayer(UserProfilePtr user);
	bool removePlayer(UserProfilePtr user);
	bool parsePlayerCommand(UserProfilePtr user, std::string command);


protected:
	// GameWorker
	class GameWorker : public Worker {
	private:
		DealerServiceWeakPtr service;

	public:
		GameWorker(DealerServicePtr service);
		virtual std::string getName() override;

	protected:
		virtual bool doWork() override;
	};
};



