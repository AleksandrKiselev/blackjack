#include "CommandToDealer.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandToDealer
CommandToDealer::CommandToDealer(Address from)
	: CommandTo(from, CommandSystem::getAddress(DealerService::DEALER_SERVICE_NAME)) {
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandAddPlayer
CommandAddPlayer::CommandAddPlayer(Address from, UserProfilePtr user)
	: CommandToDealer(from)
	, user_(user) {
}


bool CommandAddPlayer::execute(DealerServicePtr service) {
	if (nullptr != service) {
		service->addPlayer(user_);
	}
	return true;
}

std::string CommandAddPlayer::toString() {
	return "CommandAddPlayer{user='" + user_->getName() + "'}";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandRemovePlayer
CommandRemovePlayer::CommandRemovePlayer(Address from, UserProfilePtr user)
	: CommandToDealer(from)
	, user_(user) {
}


bool CommandRemovePlayer::execute(DealerServicePtr service) {
	if (nullptr != service) {
		service->removePlayer(user_);
	}
	return true;
}

std::string CommandRemovePlayer::toString() {
	return "CommandRemovePlayer{user='" + user_->getName() + "'}";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandHandleMessage
CommandHandleMessage::CommandHandleMessage(Address from, UserProfilePtr user, std::string text)
	: CommandToDealer(from)
	, text_(text)
	, user_(user) {

}

bool CommandHandleMessage::execute(DealerServicePtr service) {
	if (nullptr != service) {
		service->parsePlayerCommand(user_, text_);
	}
	return true;
}

std::string CommandHandleMessage::toString() {
	return "CommandHandleMessage{user='" + user_->getName() + "' text='" + text_ + "'}";
}
