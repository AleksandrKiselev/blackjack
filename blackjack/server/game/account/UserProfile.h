#pragma once
#include <string>
#include <memory>
#include "../GameHeader.h"

class UserProfile {
private:
	std::string name_;
	int cash_;

public:
	UserProfile(std::string name);
	UserProfile(std::string name, int cash);
	std::string getName() const;

	int getCash() const;
	void addCash(int cash);
};