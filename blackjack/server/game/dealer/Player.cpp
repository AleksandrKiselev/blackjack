#include "Player.h"
#include "../account/UserProfile.h"

Player::Player(UserProfilePtr user)
	: thinkCounter_(0)
	, state_(JOIN)
	, user_(user)
	, bet_(0) {
}


std::string Player::getName() {
	return user_->getName();
}


UserProfilePtr Player::getUser() {
	return user_;
}


deck::Hand& Player::getHand() {
	return hand_;
}


PlayerState Player::getState() {
	return state_;
}


void Player::setState(PlayerState state) {
	state_ = state;
	if ((state & SLOWDOWN) == 0)
		thinkCounter_ = 0;

	switch (state)
	{
	case WIN:
		user_->addCash(bet_);
		break;
	case LOSE:
	case BUST:
		user_->addCash((-1) * bet_);
		break;
	case JOIN:
	case OBSERVER:
		bet_ = 0;
		break;
	}
}


int Player::getBet() {
	return bet_;
}


bool Player::trySetBet(int bet) {
	if (user_->getCash() < bet)
		return false;
	bet_ = bet;
	return true;
}


int Player::getThinkCounter() {
	if ((state_ & SLOWDOWN) != 0) {
		thinkCounter_++;
	}
	return thinkCounter_;
}