// server.cpp : Defines the entry point for the console application.
//

#include <stdlib.h>     /* atoi */

#include "service\service\Service.h"
#include "service\network\TCPSocketServerConnection.h"

#include "game\dealer\DealerService.h"
#include "game\frontend\FrontendService.h"

using namespace service;


int main(int argc, char* argv[]) {
	int port = 0;
	if (argc > 1)
		port = atoi(argv[1]);
	if (port == 0)
		port = DefaultPort;

	ServerConnectionPtr connection = std::make_shared<TCPSocketServerConnection>(port);

	CommandSystem::reg(std::make_shared<FrontendService>(connection));
	CommandSystem::reg(std::make_shared<DealerService>());

	CommandSystem::setTraceCommand(true);
	CommandSystem::start();

    return 0;
}

