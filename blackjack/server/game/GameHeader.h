#pragma once
#include <unordered_map>
#include <functional>
#include <vector>
#include <memory>

#include "../service/common/Address.h"

using namespace service;


/*
 * ������� ��������� PlayerState:
 * -----------------------------------------------------------------------------------------------
 * JOIN(���������) -> (���� ��� ����)OBSERVER; (�� ����)BET
 * BET(������) -> (����� ���)OBSERVER; (�������� 'bet')START
 * START(������� �����) -> CHECK
 * CHECK(�������� ����) -> (>21)BUST; (��������)BLACKJACK; (=21)TWENTYONE; (<21, ��� �����?)THINK
 * THINK(����� ������) -> (�������� 'hit')HIT; (�������� 'stand')STAND
 * HIT(��� �����) -> CHECK
 * STAND(������ ����) -> (� ������ ������)LOSE; (� ������ ������)WIN
 * BLACKJACK(��������) -> (� ������ ������)LOSE; (� ������ ������)WIN
 * TWENTYONE(����� 21) -> (� ������ ������)LOSE; (� ������ ������)WIN
 * WIN(������� ������) -> (����� ���������� �� �����)OBSERVER
 * LOSE(�������� ������) -> (����� ���������� �� �����)OBSERVER
 * BUST(�������) -> (����� ���������� �� �����)OBSERVER
 * OBSERVER(�������� ���������, �� ��������� � ����) -> JOIN
 * -----------------------------------------------------------------------------------------------
 */

enum PlayerState {
	JOIN = 1,
	BET = 2,
	START = 4,
	CHECK = 8,
	THINK = 16,
	HIT = 32,
	STAND = 64,
	BLACKJACK = 128,
	TWENTYONE = 256,
	WIN = 512,
	LOSE = 1024,
	BUST = 2048,
	OBSERVER = 4096,

	PLAY = START | CHECK | HIT | THINK,
	SLOWDOWN = BET | THINK,
	WAITDEALERGAME = BLACKJACK | TWENTYONE | STAND,
	WAITENDGAME = WIN | LOSE | BUST,
	INGAME = PLAY | WAITDEALERGAME | WAITENDGAME,
	OUTGAME = OBSERVER | JOIN | BET
};



extern std::unordered_map<PlayerState, std::string> PlayerStateToString;

using StringVec = std::vector<std::string>;


class UserProfile;
using UserProfilePtr = std::shared_ptr<UserProfile>;
using UserProfileWeakPtr = std::weak_ptr<UserProfile>;


class Player;
using PlayerPtr = std::shared_ptr<Player>;
using PlayerWeakPtr = std::weak_ptr<Player>;
using PlayersVec = std::vector<PlayerPtr>;
using PlayersMap = std::unordered_map<UserProfilePtr, PlayerPtr>;

using PlayerStateHandler = std::function<void(PlayerPtr)>;
using PlayerStateHandlers = std::unordered_map<PlayerState, PlayerStateHandler>;


using PlayerCommandHandler = std::function<void(PlayerPtr, StringVec)>;
struct PlayerCommand {
	std::string description;
	PlayerCommandHandler handler;
};
using PlayerCommands = std::unordered_map<std::string, PlayerCommand>;


class DealerService;
using DealerServicePtr = std::shared_ptr<DealerService>;
using DealerServiceWeakPtr = std::weak_ptr<DealerService>;


class FrontendWorker;
using FrontendWorkerPtr = std::shared_ptr<FrontendWorker>;
using FrontendWorkerWeakPtr = std::weak_ptr<FrontendWorker>;


class FrontendService;
using FrontendServicePtr = std::shared_ptr<FrontendService>;
using FrontendServiceWeakPtr = std::weak_ptr<FrontendService>;
