#pragma once
#include "../ServiceHeader.h"
#include "../service/Service.h"


namespace service {

	template <typename F, typename... Ts>
	inline auto reallyAsync(F&& func, Ts&&... params) {
		return std::async(
			std::launch::async,
			std::forward<F>(func),
			std::forward<Ts>(params)...
		);
	}



	class CommandService : public Service {
	private:
		int workersCount_;

	public:
		CommandService(int workersCount = 1);

	protected:
		bool initialize() override;
	};



	class CommandWorker : public Worker {
	private:
		CommandServiceWeakPtr service_;

	public:
		CommandWorker(CommandServicePtr service);
		virtual std::string getName() override;

	protected:
		bool doWork() override;
	};
}
