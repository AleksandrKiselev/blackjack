#pragma once
#include "FrontendService.h"
#include "../../service/command/Command.h"
#include "../../service/service/Service.h"


class CommandToFrontend : public CommandTo<FrontendService> {
public:
	CommandToFrontend(Address from);
};


class CommandBroadcast : public CommandToFrontend {
private:
	UserProfilePtr except_;
	std::string text_;

public:
	CommandBroadcast(Address from, UserProfilePtr except, std::string text);
	virtual bool execute(FrontendServicePtr service);
	virtual std::string toString() override;
};


class CommandSend : public CommandToFrontend {
private:
	UserProfilePtr to_;
	std::string text_;

public:
	CommandSend(Address from, UserProfilePtr to_, std::string text);
	virtual bool execute(FrontendServicePtr service);
	virtual std::string toString() override;
};


class CommandDisconnect : public CommandToFrontend {
private:
	UserProfilePtr user_;

public:
	CommandDisconnect(Address from, UserProfilePtr user_);
	virtual bool execute(FrontendServicePtr service);
	virtual std::string toString() override;
};

