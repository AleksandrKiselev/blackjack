#include <ctime>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <windows.h>
#include <time.h>

#include "Logger.h"


namespace service {

	bool isDirExists(const std::string& dirName) {
		DWORD ftyp = GetFileAttributesA(dirName.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;

		return false;
	}


	std::string getThreadId() {
		std::stringstream ss;
		ss << GetCurrentThreadId();
		return ss.str();
	}

#pragma warning(disable: 4996) /* Disable _CRT_SECURE_NO_WARNINGS  */

	std::string getTime(bool fsFormat) {
		std::time_t t = std::time(nullptr);
		std::tm tm = *std::localtime(&t);

		std::stringstream ss;
		ss << std::put_time(&tm, fsFormat ? "%Y_%m_%d_%H_%M_%S" : "%Y-%m-%d %H:%M:%S");
		return ss.str();
	}

#pragma warning(default: 4996) /* Restore _CRT_SECURE_NO_WARNINGS  */



	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// LoggerManager
	LoggerManager::LoggerManager() {
		if (!isDirExists(OUT_FOLDER.c_str()))
			CreateDirectoryA(OUT_FOLDER.c_str(), NULL);

		std::string fname(OUT_FOLDER + "\\" + getTime(true) + EXTENSION);
		fout_.open(fname, std::fstream::out | std::fstream::trunc);
	}


	LoggerManager::~LoggerManager() {
		std::lock_guard<std::mutex> lock(mutex_);
		if (fout_.is_open()) {
			fout_.close();
		}
	}


	void LoggerManager::write(const std::string& text) {
		std::lock_guard<std::mutex> lock(mutex_);

		std::string formated = "[" + getTime() + ", " + getThreadId() + "]\t" + text;
		std::cout << formated << std::endl;

		if (fout_.is_open()) {
			fout_ << formated << std::endl;
		}
	}


	LoggerPtr LoggerManager::getLogger(const std::string& name) {
		static LoggerManager i;

		std::lock_guard<std::mutex> lock(i.mutex_);
		if (!i.loggers_.count(name)) {
			i.loggers_[name] = std::make_shared<Logger>(i, name);
		}

		return i.loggers_[name];
	}



	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Logger
	Logger::Logger(LoggerManager& manager, const std::string& name)
		: manager_(manager)
		, name_(name) {
	}


	void Logger::info(const std::string& text) {
		manager_.write(INFO_PREFIX + name_ + "\t\t" + text);
	}


	void Logger::error(const std::string& text) {
		manager_.write(ERROR_PREFIX + name_ + "\t\t" + text);
	}


	const LoggerPtr& operator<<(LoggerPtr& logger, const std::string& text) {
		logger->info(text);
		return logger;
	}

}