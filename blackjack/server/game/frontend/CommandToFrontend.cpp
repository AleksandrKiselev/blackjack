#include "CommandToFrontend.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandToFrontend
CommandToFrontend::CommandToFrontend(Address from)
	: CommandTo(from, CommandSystem::getAddress(FrontendService::FRONTEND_SERVICE_NAME)) {
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandBroadcast
CommandBroadcast::CommandBroadcast(Address from, UserProfilePtr except, std::string text)
	: CommandToFrontend(from)
	, except_(except)
	, text_(text) {
}


bool CommandBroadcast::execute(FrontendServicePtr service) {
	if (nullptr != service) {
		service->broadcast(except_, text_);
		return false;
	}
	return true;
}


std::string CommandBroadcast::toString() {
	std::string result("CommandBroadcast{");
	if (except_ != nullptr) {
		result += "except='" + except_->getName() + "' ";
	}
	result += "text='" + text_ + "'}";
	return result;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandSend
CommandSend::CommandSend(Address from, UserProfilePtr to, std::string text)
	: CommandToFrontend(from)
	, text_(text)
	, to_(to) {
}


bool CommandSend::execute(FrontendServicePtr service) {
	if (nullptr != service) {
		service->send(to_, text_);
	}
	return true;
}


std::string CommandSend::toString() {
	return "CommandSend{to='" + to_->getName() +"' text='" + text_ + "'}";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// CommandDisconnect
CommandDisconnect::CommandDisconnect(Address from, UserProfilePtr user) 
	: CommandToFrontend(from)
	, user_(user) {
}


bool CommandDisconnect::execute(FrontendServicePtr service) {
	if (nullptr != service) {
		service->disconnect(user_);
	}
	return true;
}


std::string CommandDisconnect::toString() {
	return "CommandDisconnect{to='" + user_->getName() + "'}";
}