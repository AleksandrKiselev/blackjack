// client.cpp : Defines the entry point for the console application.
//

#include <string>
#include <iostream>
#include "ClientService.h"

using namespace service;

int main(int argc, char* argv[]) {
	std::string host("127.0.0.1");
	int port = 8083;
	if (argc > 2) {
		host = argv[1];
		port = atoi(argv[2]);
	}

	std::cout << "Host: " << host << " Port: " << port << std::endl;

	CommandSystem::reg(std::make_shared<ClientService>(host, port));
	CommandSystem::start(false);

	Address console;
	while (true) {
		std::string text;
		std::getline(std::cin, text);
		CommandSystem::send(new CommandSend(console, text));
	}

    return 0;
}

