#pragma once
#include <atomic>


namespace service {
	class Address final {
	private:
		static std::atomic_int idCreator_;
		int id_;

	public:
		static const Address EMPTY;

	public:
		Address() : id_(idCreator_++) {}
		explicit Address(int id) : id_(id) {}

		Address(const Address& address) = default;
		~Address() = default;

		bool operator==(const Address& address) const { return id_ == address.id_; }
		int getId() const { return id_; }
	};
}


namespace std {
	template <>
	struct hash<service::Address> {
		std::size_t operator()(const service::Address& k) const {
			return k.getId();
		}
	};
}
