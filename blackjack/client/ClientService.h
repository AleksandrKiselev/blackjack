#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment (lib, "ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <ws2ipdef.h>

#include <unordered_map>
#include <functional>
#include <iostream>                                                                                                              
#include <vector>   
#include <string>     
#include <set>  

#include "service/command/CommandService.h"
#include "service/command/Command.h"


using namespace service;

class ClientService;
using ClientServicePtr = std::shared_ptr<ClientService>;
using ClientServiceWeakPtr = std::weak_ptr<ClientService>;


class ClientService : public CommandService {
public:
	static const std::string CLIENT_SERVICE_NAME;


private:
	std::string host_;
	SOCKET socket_;
	int port_;


public:
	ClientService(std::string host, int port, int workersCount = 1);
	virtual std::string getName() override;

	bool write(std::string text);
	std::string receive();


protected:
	virtual bool initialize() override;


	// ClientWorker
	class ClientWorker : public Worker {
	private:
		ClientServiceWeakPtr service_;

	public:
		ClientWorker(ClientServicePtr service);
		virtual std::string getName() override;

	protected:
		virtual bool doWork() override;
	};
};




class CommandToClient : public CommandTo<ClientService> {
public:
	CommandToClient(Address from);
};


class CommandSend : public CommandToClient {
private:
	std::string text_;

public:
	CommandSend(Address from, std::string text);
	virtual bool execute(ClientServicePtr service);
	virtual std::string toString() override;
};