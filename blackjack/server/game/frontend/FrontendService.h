#pragma once
#include <regex>
#include <mutex>

#include "../GameHeader.h"
#include "../account/UserProfile.h"
#include "../../service/command/CommandService.h"
#include "../../service/network/ServerConnection.h"


class FrontendService : public CommandService {
public:
	static const std::string FRONTEND_SERVICE_NAME;

private:
	std::unordered_map<ConnectionId, UserProfilePtr> sessions_;
	ServerConnectionPtr connection_;
	std::recursive_mutex mutex_;
	std::regex nameRegex_;


public:
	FrontendService(ServerConnectionPtr connection, int workersCount = 1);
	virtual std::string getName() override;

	// frontend service functionality
	bool handleConnectionActivity();

	void broadcast(UserProfilePtr except, std::string text);
	void broadcast(ConnectionId except, std::string text);
	void send(UserProfilePtr to, std::string text);	
	void send(ConnectionId to, std::string text);

	void disconnect(UserProfilePtr user);


protected:
	virtual bool initialize() override;

	ServerConnectionPtr getConnection();

	// work with sessions
	bool isExistName(std::string name);
	bool isValidName(std::string name);

	UserProfilePtr findUser(ConnectionId id);
	ConnectionId findConn(UserProfilePtr user);


	// FrontendWorker
	class FrontendWorker : public Worker {
	private:
		FrontendServiceWeakPtr service;

	public:
		FrontendWorker(FrontendServicePtr service);
		virtual std::string getName() override;

	protected:
		virtual bool doWork() override;
	};

};

