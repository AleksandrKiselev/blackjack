#pragma once
#include <atomic>
#include <thread>

#include "../ServiceHeader.h"

namespace service {


	// Base worker class
	class Worker : public Named, private NonCopyable {
	private:
		std::atomic_bool isWorking_ = false;
		std::thread thread_;

	public:
		Worker() = default;
		virtual ~Worker() = default;

		void start();
		void join();
		void stop();

	protected:
		virtual void threadFunction();
		virtual bool doWork() = 0;
	};



	// Base service class
	class Service : public Named, private NonCopyable, public SharedFromThis<Service> {
	protected:
		WorkersVec workers_;
		Address address_;	

	public:
		Service() = default;
		virtual ~Service() = default;

		Address getAddress();
		void start();
		void join();
		void stop();

	protected:
		virtual bool initialize() = 0;
	};



	// Contains list of services and their command queues
	class CommandSystem : private NonCopyable {
	private:
		std::atomic_bool isTraceCommand_ = false;
		std::atomic_bool isWorking_ = false;
		CommandsMap commands_;
		ServicesMap services_;
		

		// singleton
		CommandSystem() = default;
		static CommandSystem& getInstance() {
			static CommandSystem instance;
			return instance;
		}


	public:
		virtual ~CommandSystem();

		static Address getAddress(const std::string& serviceName);
		static bool reg(ServicePtr service);
		static void start(bool bJoin = true);
		static void stop(bool bJoin = true);

		static void send(Command* command);
		static CommandPtr receive(Address address);

		static void setTraceCommand(bool trace);
		static bool getTraceCommand();
	};
}
