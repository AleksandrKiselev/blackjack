#pragma once
#include <string>
#include "Logger.h"


namespace service {

	class Named {
	private:
		LoggerPtr logger_;


	public:
		Named() : logger_(nullptr) {}
		virtual ~Named() = default;

		LoggerPtr getLogger() {
			if (nullptr == logger_) {
				logger_ = LoggerManager::getLogger(getName());
			}
			return logger_;
		}

		virtual std::string getName() = 0;
	};

}
