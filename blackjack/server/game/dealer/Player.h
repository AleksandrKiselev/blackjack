#pragma once
#include "../GameHeader.h"
#include "Deck.h"

class Player {
private:
	UserProfilePtr user_;
	PlayerState state_;
	deck::Hand hand_;
	int bet_;

	int thinkCounter_;

public:
	Player(UserProfilePtr user);

	std::string getName();
	UserProfilePtr getUser();
	deck::Hand& getHand();

	PlayerState getState();
	void setState(PlayerState state);

	int getBet();
	bool trySetBet(int bet);

	int getThinkCounter();
};
