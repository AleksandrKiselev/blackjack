#pragma once
#include "../ServiceHeader.h"
#include <type_traits>
#include <assert.h>


namespace service {

	// Base command class
	class Command : private NonCopyable {
	private:
		Address from_;
		Address to_;

	public:
		Command(Address from, Address to);
		virtual ~Command() = default;

		Address getFrom();
		Address getTo();

		virtual bool execute(ServicePtr service) = 0;
		virtual std::string toString() = 0;
	};



	// Command to concrete service template
	template <typename ST>
	class CommandTo : public Command {
	public:
		CommandTo(Address from, Address to) 
			: Command(from, to) {
			static_assert(std::is_base_of<Service, ST>::value, "template type must be derived by class 'Service'");
		};
		virtual ~CommandTo() = default;

		// concrete service must override this
		virtual bool execute(std::shared_ptr<ST> service) = 0;

		virtual bool execute(ServicePtr service) override {
			std::shared_ptr<ST> concreteService = std::dynamic_pointer_cast<ST>(service);
			if (nullptr == concreteService) {
				return false;
			}
			return execute(concreteService);
		}
	};
}