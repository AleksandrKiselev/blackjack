#pragma once
#include "Deck.h"
#include "../GameHeader.h"
#include "../../service/common/Address.h"


using namespace service;

// class to communicate with players
class DealerFeedbackProcessor {
private:
	Address address_;

		
public:
	DealerFeedbackProcessor(Address address);

	void sendTo(PlayerPtr player, std::string text, bool decorate = false);
	void sendAll(PlayerPtr player, std::string text, bool decorate = false);

	void sendWelcome(PlayerPtr player, bool isGameStarted);
	void sendGoodbye(PlayerPtr player);

	void sendStartGame();
	void sendGameResults(PlayersVec players);	

	void sendBet(PlayerPtr player);
	void sendStand(PlayerPtr player);
	void sendPass(PlayerPtr player);

	void sendHand(PlayerPtr player);
	void sendTakenCard(PlayerPtr player, const deck::Card& card, bool toAll = true);
	void sendOfferCard(PlayerPtr player);

	void sendInfoAbout(PlayerPtr player, bool hand = false, bool toAll = false);
	void sendInfoAbout(PlayersVec players);

	std::string getInfo(PlayerPtr player, bool hand = true);
	std::string getHand(PlayerPtr player);
	std::string decorate(std::string text, bool thickBorder = false);
};
