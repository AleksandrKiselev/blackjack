#include "ClientService.h"

const std::string ClientService::CLIENT_SERVICE_NAME("ClientService");

ClientService::ClientService(std::string host, int port, int workersCount)
	: CommandService(workersCount)
	, socket_(INVALID_SOCKET)
	, host_(host)
	, port_(port) {

}


std::string ClientService::getName() {
	return CLIENT_SERVICE_NAME;
}


bool ClientService::initialize() {
	WSADATA wsaData;
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		getLogger() << "WSAStartup failed with error: " + std::to_string(iResult);
		return false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(host_.c_str(), std::to_string(port_).c_str(), &hints, &result);
	if (iResult != 0) {
		getLogger() << "getaddrinfo failed with error: " + std::to_string(iResult);
		WSACleanup();
		return false;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		socket_ = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (socket_ == INVALID_SOCKET) {
			getLogger() << "socket failed with error: " + std::to_string(WSAGetLastError());
			WSACleanup();
			return false;
		}

		// Connect to server.
		iResult = connect(socket_, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			getLogger() << "connect error: " + std::to_string(WSAGetLastError());
			closesocket(socket_);
			socket_ = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (socket_ == INVALID_SOCKET) {
		getLogger() << "Unable to connect to server!";
		WSACleanup();
		return false;
	}

	// Create client worker
	auto sharedThis = downcasted_shared_from_this<ClientService>();
	workers_.emplace_back(new ClientWorker(sharedThis));

	return CommandService::initialize();
}


bool ClientService::write(std::string text) {
	int iResult = send(socket_, text.c_str(), (int)text.size(), 0);
	if (iResult == SOCKET_ERROR) {
		getLogger() << "send failed with error: " + std::to_string(WSAGetLastError());
		closesocket(socket_);
		WSACleanup();
		stop();
		return false;
	}

	//getLogger() << "Bytes Sent: " + std::to_string(iResult);
	return true;
}


std::string ClientService::receive() {
	char recvbuf[1024] = { 0 };
	int iResult = recv(socket_, recvbuf, 1023, 0);
	if (iResult > 0) {
		//getLogger() << "Bytes received: " + std::to_string(iResult);
		return std::string(recvbuf);
	}
	else if (iResult == 0)
		getLogger() << "Connection closed\n";
	else
		getLogger() << "recv failed with error: " + std::to_string(WSAGetLastError());
	stop();
	return "";
}




ClientService::ClientWorker::ClientWorker(ClientServicePtr service)
	: service_(service) {
}


std::string ClientService::ClientWorker::getName() {
	return "ClientWorker";
}

bool ClientService::ClientWorker::doWork() {
	if (ClientServicePtr servLock = service_.lock()) {
		std::cout << servLock->receive();
		return true;
	}
	return false;
}



CommandToClient::CommandToClient(Address from)
	: CommandTo(from, CommandSystem::getAddress(ClientService::CLIENT_SERVICE_NAME)) {
}



CommandSend::CommandSend(Address from, std::string text) 
	: CommandToClient(from)
	, text_(text) {
}

bool CommandSend::execute(ClientServicePtr service) {
	if (nullptr != service) {
		service->write(text_);
	}
	return true;
}

std::string CommandSend::toString() {
	return "CommandSend{text='" + text_ + "'}";
}