#pragma once
#include <unordered_map>
#include <random>
#include <string>
#include <vector>
#include <deque>


namespace deck {

	enum class Suit : int {
		HEART   = 0,
		DIAMOND = 1,
		CLUB    = 2,
		SPADE   = 3
	};

	enum class Rank : int {
		ACE    = 0,
		KING   = 1,
		QUEEN  = 2,
		JACK   = 3,
		TEN    = 4,
		NINE   = 5,
		EIGHT  = 6,
		SEVEN  = 7,
		SIX    = 8,
		FIVE   = 9,
		FOUR   = 10,
		THREE  = 11,
		TWO    = 12
	};

	struct RankInfo {
		std::string name;
		int points;
	};



	// Card
	class Card {
	public:
		Suit suit;
		Rank rank;

	public:
		Card(Suit suit, Rank rank);
		int getPoints() const;
		std::string getName() const;
	};



	// Hand
	class Hand {
	public:
		std::vector<Card> cards;

	public:
		void add(Card card);
		void drop();

		bool isBlackjack() const;
		bool isTwentyOne() const;
		bool isBust() const;

		int getScore() const;

		std::string getScoreString() const;
		std::string toString() const;
	};



	// Deck
	class Deck {
	private:
		std::vector<Card> cards_;
		std::deque<int> order_;
		int count_ = 0;

		bool isShuffleMachine_ = true;

	public:
		Deck();

		void shuffle(bool reset = true);
		Card pop();
	};
}
