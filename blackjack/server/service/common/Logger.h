#pragma once
#include <mutex>
#include <string>
#include <fstream>
#include <unordered_map>

#include "NonCopyable.h"


namespace service {

	bool isDirExists(const std::string& dirName);

	std::string getTime(bool fsFormat = false);

	std::string getThreadId();
	

	class Logger;
	using LoggerPtr = std::shared_ptr<Logger>;



	// LoggerManager
	class LoggerManager : private NonCopyable {
	private:
		friend class Logger;

		const std::string OUT_FOLDER = ".\\out";
		const std::string EXTENSION = ".log";

		std::unordered_map<std::string, LoggerPtr> loggers_;
		std::ofstream fout_;
		std::mutex mutex_;


	private:
		// singleton
		LoggerManager();
		void write(const std::string& text);


	public:
		virtual ~LoggerManager();
		static LoggerPtr getLogger(const std::string& name);
	};



	// Logger
	class Logger : private NonCopyable {
	private:
		const std::string INFO_PREFIX = "INFO\t";
		const std::string ERROR_PREFIX = "ERROR\t";

		LoggerManager& manager_;
		std::string name_;


	public:
		Logger(LoggerManager& manager, const std::string& name);
		virtual ~Logger() = default;

		void info(const std::string& text);
		void error(const std::string& text);
	};


	const LoggerPtr& operator<<(LoggerPtr& logger, const std::string& text);

}

