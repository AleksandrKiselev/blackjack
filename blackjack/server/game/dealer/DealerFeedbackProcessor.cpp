#include "DealerFeedbackProcessor.h"
#include "DealerService.h"
#include "../frontend/CommandToFrontend.h"


DealerFeedbackProcessor::DealerFeedbackProcessor(Address address)
	: address_(address) {
}


void DealerFeedbackProcessor::sendTo(PlayerPtr player, std::string text, bool decorate) {
	if (nullptr != player) {
		if (decorate) text = DealerFeedbackProcessor::decorate(text);
		CommandSystem::send(new CommandSend(address_, player->getUser(), text + "\n"));
	}
}


void DealerFeedbackProcessor::sendAll(PlayerPtr player, std::string text, bool decorate) {
	UserProfilePtr user = nullptr;
	if (nullptr != player) user = player->getUser();

	if (decorate) text = DealerFeedbackProcessor::decorate(text);
	CommandSystem::send(new CommandBroadcast(address_, user, text + "\n"));
}


void DealerFeedbackProcessor::sendWelcome(PlayerPtr player, bool isGameStarted) {
	if (nullptr != player) {
		std::string text(player->getName());
		text += ", welcome to the card game \"Blackjack\"\n";
		if (isGameStarted) {
			text += "Game was already started, wait for a next";
		}
		sendTo(player, decorate(text, true));
	}
}


void DealerFeedbackProcessor::sendGoodbye(PlayerPtr player) {
	if (nullptr != player) {
		std::string text(player->getName());
		text += " left game\n";
		sendAll(player, text);
	}
}


void DealerFeedbackProcessor::sendInfoAbout(PlayerPtr player, bool hand, bool toAll) {
	std::string text = getInfo(player, hand);
	toAll ? sendAll(nullptr, text) : sendTo(player, text);
}


void DealerFeedbackProcessor::sendInfoAbout(PlayersVec players) {
		std::string text;
		for (auto p : players) {
			text += getInfo(p);
			text += "\n";
		}
		sendAll(nullptr, text);
}


void DealerFeedbackProcessor::sendStartGame() {
	std::string text(decorate("NEW GAME", true));
	text += "Place your bets (command 'bet <number>'):";
	sendAll(nullptr, text);
}


void DealerFeedbackProcessor::sendGameResults(PlayersVec players) {
	std::string text("Results:\n");
	for (auto p : players) {
		text += getInfo(p, false);
		text += "\n";
	}
 	sendAll(nullptr, decorate(text));
}


void DealerFeedbackProcessor::sendBet(PlayerPtr player) {
	if (nullptr != player) {
		std::string text(player->getName());
		text += " placed the bet ";
		text += std::to_string(player->getBet());

		sendAll(nullptr, text);
	}
}


void DealerFeedbackProcessor::sendStand(PlayerPtr player) {
	if (nullptr != player) {
		sendAll(nullptr, player->getName() + " don't take more cards");
	}
}


void DealerFeedbackProcessor::sendPass(PlayerPtr player) {
	if (nullptr != player) {
		sendAll(nullptr, player->getName() + " pass this game");
	}
}


void DealerFeedbackProcessor::sendHand(PlayerPtr player) {
	if (nullptr != player) {
		sendTo(player, decorate(player->getHand().toString()));
	}
}


void DealerFeedbackProcessor::sendTakenCard(PlayerPtr player, const deck::Card& card, bool toAll) {
	if (nullptr != player) {

		std::string text("You take ");
		text += card.getName();
		text += " / ";
		text += player->getHand().getScoreString();

		sendTo(player, text);

		if (toAll) {
			std::string text(player->getName());
			text += " take ";
			text += card.getName();
			text += " / ";
			text += player->getHand().getScoreString();

			sendAll(player, text);
		}
	}
}


void DealerFeedbackProcessor::sendOfferCard(PlayerPtr player) {
	if (nullptr != player) {
		std::string text(getHand(player));
		text += "\nTake another card? (hit/stand)";

		sendTo(player, decorate(text));
	}
}


std::string DealerFeedbackProcessor::getInfo(PlayerPtr player, bool hand) {
	std::string text;
	if (nullptr != player) {
		text += "name: ";
		text += player->getName();
		text += " / state: ";
		text += PlayerStateToString[player->getState()];
		if (!hand) {
			text += " / ";
			text += player->getHand().getScoreString();
		}
		text += " / bet: ";
		text += std::to_string(player->getBet());
		text += " / cash: ";
		text += std::to_string(player->getUser()->getCash());
		if (hand) {
			text += "\n";
			text += getHand(player);
		}
	}
	return text;
}


std::string DealerFeedbackProcessor::getHand(PlayerPtr player) {
	std::string text;
	if (nullptr != player) {
		text += "hand:\n";
		text += player->getHand().toString();
	}
	return text;
}


std::string DealerFeedbackProcessor::decorate(std::string text, bool thickBorder) {
	std::string border = thickBorder ?
		"\n///////////////////////////////////////////\n" :
		"\n-------------------------------------------\n";

	return border + text + border;
}
