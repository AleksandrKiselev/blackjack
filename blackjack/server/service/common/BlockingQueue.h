#pragma once
#include <condition_variable>
#include <mutex>
#include <deque>

#include "NonCopyable.h"


namespace service {

	template <typename T>
	class BlockingQueue : private NonCopyable {
	private:
		std::condition_variable cvar_;
		mutable std::mutex mutex_;
		std::deque<std::unique_ptr<T>> queue_;
		

	public:
		BlockingQueue() = default;
		virtual ~BlockingQueue() = default;


		void push(T* value) {
			std::lock_guard<std::mutex> lock(mutex_);
			queue_.emplace_back(value);
			cvar_.notify_one();
		}


		std::unique_ptr<T> pop() {
			{
				std::unique_lock<std::mutex> lock(mutex_);
				cvar_.wait_for(lock, std::chrono::milliseconds(100), [&]() {
					return !queue_.empty();
				});
			}

			std::lock_guard<std::mutex> lock(mutex_);
			if (queue_.empty()) 
				return nullptr;

			std::unique_ptr<T> front;
			front.swap(queue_.front());
			queue_.pop_front();
			return front;
		}


		size_t size() const {
			std::lock_guard<std::mutex> lock(mutex_);
			return queue_.size();
		}
	};

}