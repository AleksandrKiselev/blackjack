#pragma once
#include <memory>
#include <vector>
#include <string>
#include <unordered_map>

#include "common\Named.h"
#include "common\Logger.h"
#include "common\Address.h"
#include "common\NonCopyable.h"
#include "common\BlockingQueue.h"
#include "common\SharedFromThis.h"

namespace service {

	class Worker;
	using WorkerPtr = std::shared_ptr<Worker>;
	using WorkersVec = std::vector<WorkerPtr>;


	class Service;
	using ServicePtr = std::shared_ptr<Service>;
	using ServiceWeakPtr = std::weak_ptr<Service>;


	class Command;
	using CommandPtr = std::unique_ptr<Command>;


	// Command blocking queue
	using CommandQueue = BlockingQueue<Command>;
	using CommandQueuePtr = std::unique_ptr<CommandQueue>;


	class CommandWorker;
	using CommandWorkerPtr = std::shared_ptr<CommandWorker>;


	class CommandService;
	using CommandServicePtr = std::shared_ptr<CommandService>;
	using CommandServiceWeakPtr = std::weak_ptr<CommandService>;


	using CommandsMap = std::unordered_map<Address, CommandQueuePtr>;
	using ServicesMap = std::unordered_map<ServicePtr, Address>;
}
