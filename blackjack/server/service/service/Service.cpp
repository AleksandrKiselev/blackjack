#include "Service.h"
#include "../command/Command.h"


namespace service {

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Worker
	void Worker::start() {
		isWorking_ = true;
		auto fn = std::mem_fn(&Worker::threadFunction);
		thread_ = std::thread(fn, this);
	}


	void Worker::join() {
		if (thread_.joinable()) {
			thread_.join();
		}
	}


	void Worker::stop() {
		isWorking_ = false;
	}


	void Worker::threadFunction() {
		while (isWorking_) {
			try { 
				if (!doWork()) break;
			}

			catch (const std::runtime_error& re) {
				getLogger() << "Runtime error: " + std::string(re.what());
			}

			catch (const std::exception& ex) {
				getLogger() << "Error occurred: " + std::string(ex.what());
			}

			catch (...) {
				getLogger() << "Unknown failure occurred. Possible memory corruption";
			}
		}
		isWorking_ = false;
	}



	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// Service
	Address Service::getAddress() {
		return address_; 
	}


	void Service::start() {
		if (!initialize()) return;
		getLogger() << "started";

		for (auto w : workers_) {
			w->start();
		}
	}


	void Service::stop() {
		getLogger() << "stopped";

		for (auto w : workers_) {
			w->stop();
		}
	}


	void Service::join() {
		for (auto w : workers_) {
			w->join();
		}
	}



	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// CommandSystem
	CommandSystem::~CommandSystem() {
		stop();
	}


	bool CommandSystem::reg(ServicePtr service) {
		CommandSystem& i = getInstance();

		// registration is available when the services are not running
		if (i.isWorking_ || i.services_.count(service))
			return false;

		Address address = service->getAddress();
		i.services_[service] = address;
		i.commands_[address] = std::make_unique<CommandQueue>();

		return true;
	}


	void CommandSystem::start(bool bJoin) {
		CommandSystem& i = getInstance();
		if (i.isWorking_) return;
		i.isWorking_ = true;

		for (auto e : i.services_) {
			e.first->start();
		}

		if (!bJoin) return;
		for (auto e : i.services_) {
			e.first->join();
		}
	}
	


	void CommandSystem::stop(bool bJoin) {
		CommandSystem& i = getInstance();
		if (!i.isWorking_) return;

		for (auto e : i.services_) {
			e.first->stop();
		}

		if (!bJoin) return;
		for (auto e : i.services_) {
			e.first->join();
		}

	}


	Address CommandSystem::getAddress(const std::string& name) {
		CommandSystem& i = getInstance();

		for (auto e : i.services_) {
			if (e.first->getName() == name) {
				return e.second;
			}
		}
		return Address::EMPTY;
	}


	void CommandSystem::send(Command* command) {
		CommandSystem& i = getInstance();
		if (!i.isWorking_) return;

		Address to = command->getTo();
		auto it = i.commands_.find(to);
		if (it == i.commands_.end()) return;

		it->second->push(command);
	}


	CommandPtr CommandSystem::receive(Address address) {
		CommandSystem& i = getInstance();
		if (!i.isWorking_) return nullptr;

		auto it = i.commands_.find(address);
		if (it == i.commands_.end()) return nullptr;

		return it->second->pop();
	}



	void CommandSystem::setTraceCommand(bool trace) {
		CommandSystem& i = getInstance();
		i.isTraceCommand_ = trace;
	}


	bool CommandSystem::getTraceCommand() {
		CommandSystem& i = getInstance();
		return i.isTraceCommand_;
	}

}
