#include "FrontendService.h"
#include "CommandToFrontend.h"
#include "../dealer/CommandToDealer.h"
#include "../../service/network/TCPSocketServerConnection.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////
// FrontendService

const std::string FrontendService::FRONTEND_SERVICE_NAME("FrontendService");

FrontendService::FrontendService(ServerConnectionPtr connection, int workersCount)
	: CommandService(workersCount)
	, connection_(connection)
	, nameRegex_("^[A-Za-z0-9_]{3,30}$") {
}


std::string FrontendService::getName() {
	return FRONTEND_SERVICE_NAME;
}


bool FrontendService::initialize() {
	ServerConnectionPtr conn = getConnection();

	conn->setOnConnect(
		[&](ConnectionId id) {
			getLogger() << id + " connected";
			send(id, "Enter your name: ");
		}
	);


	conn->setOnClose(
		[&](ConnectionId id) {
			getLogger() << id + " disconnected";
			if (UserProfilePtr user = findUser(id)) {
				CommandSystem::send(new CommandRemovePlayer(getAddress(), user));
				sessions_.erase(id);
			}
		}
	);


	conn->setOnRead(
		[&](ConnectionId id, std::string text) {
			UserProfilePtr user = findUser(id);
			if (nullptr == user) {
				if (!isValidName(text)) {
					send(id, "Incorrect name. Allowed only letters, numbers and '_'. (3-30 characters)");
					return;
				}

				if (isExistName(text)) {
					send(id, "Name already in use");
					return;
				}

				user = std::make_shared<UserProfile>(text);
				sessions_[id] = user;
				CommandSystem::send(new CommandAddPlayer(getAddress(), user));
				return;
			}

			CommandSystem::send(new CommandHandleMessage(getAddress(), user, text));
		}
	);


	conn->setOnThrow(
		[&](std::string text) {
			getLogger() << "Connection error: " + text;
		}
	);


	if (!conn->initialize())
		return false;

	// add frontend worker
	auto sharedThis = downcasted_shared_from_this<FrontendService>();
	workers_.emplace_back(new FrontendWorker(sharedThis));

	// add command workers
	return CommandService::initialize();
}


ServerConnectionPtr FrontendService::getConnection() {
	if (nullptr == connection_) {
		connection_ = std::make_shared<TCPSocketServerConnection>();
	}
	return connection_;
}


bool FrontendService::isExistName(std::string name) {
	for (auto e : sessions_) {
		if (e.second->getName() == name) {
			return true;
		}
	}
	return false;
}


bool FrontendService::isValidName(std::string name) {
	return std::regex_match(name, nameRegex_);
}


UserProfilePtr FrontendService::findUser(ConnectionId id) {
	auto it = sessions_.find(id);
	if (it != sessions_.end()) {
		return it->second;
	}

	return nullptr;
}


ConnectionId FrontendService::findConn(UserProfilePtr user) {
	for (auto e : sessions_) {
		if (e.second == user) {
			return e.first;
		}
	}
	return std::string();
}



// frontend service functionality

bool FrontendService::handleConnectionActivity() {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	return getConnection()->handleConnectionActivity();
}

void FrontendService::broadcast(ConnectionId except, std::string text) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	for (auto s : sessions_) {
		if (s.first == except) continue;
		send(s.first, text);
	}
}


void FrontendService::send(ConnectionId to, std::string text) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	getConnection()->write(to, text);
}


void FrontendService::broadcast(UserProfilePtr except, std::string text) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	ConnectionId conn = findConn(except);
	broadcast(conn, text);
}


void FrontendService::send(UserProfilePtr to, std::string text) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	ConnectionId conn = findConn(to);
	if (!conn.empty()) {
		send(conn, text);
	}
}


void FrontendService::disconnect(UserProfilePtr user) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);

	ConnectionId conn = findConn(user);
	if (!conn.empty()) {
		getConnection()->disconnect(conn);
	}
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
// FrontendService
FrontendService::FrontendWorker::FrontendWorker(FrontendServicePtr service)
	: service(service) {
}


std::string FrontendService::FrontendWorker::getName() {
	return "FrontendWorker";
}


bool FrontendService::FrontendWorker::doWork() {
	if (FrontendServicePtr servLock = service.lock()) {
		return servLock->getConnection()->handleConnectionActivity();
	}
	return false;
}
