#include "Command.h"

namespace service {

	Command::Command(Address from, Address to) 
		: from_(from)
		, to_(to) {
	}

	Address Command::getFrom() {
		return from_; 
	}

	Address Command::getTo() {
		return to_; 
	}

	bool Command::execute(ServicePtr service) {
		return false; 
	}

	std::string Command::toString() { 
		return "Command{}"; 
	}

}