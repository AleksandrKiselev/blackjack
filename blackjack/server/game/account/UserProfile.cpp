#include "UserProfile.h"

UserProfile::UserProfile(std::string name)
	: name_(name)
	, cash_(1000) {
}

UserProfile::UserProfile(std::string name, int cash) 
	: name_(name)
	, cash_(cash) {
}

std::string UserProfile::getName() const {
	return name_;
}

int UserProfile::getCash() const {
	return cash_;
}

void UserProfile::addCash(int cash) {
	cash_ += cash;
}

namespace std {
	template <>
	struct hash<UserProfilePtr> {
		std::size_t operator()(const UserProfilePtr& k) const {
			std::hash<std::string> hash_fn;
			return hash_fn(k->getName());
		}
	};
}