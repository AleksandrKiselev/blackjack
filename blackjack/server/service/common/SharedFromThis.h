#pragma once
#include <type_traits>
#include <memory>



namespace service {

	class VirtualEnableSharedFromThis : public std::enable_shared_from_this<VirtualEnableSharedFromThis> {
	public:
		virtual ~VirtualEnableSharedFromThis() = default;
	};


	template <typename T>
	class SharedFromThis : virtual public VirtualEnableSharedFromThis {
	public:
		std::shared_ptr<T> shared_from_this() {
			return std::dynamic_pointer_cast<T>(VirtualEnableSharedFromThis::shared_from_this());
		}

		template <typename Down>
		std::shared_ptr<Down> downcasted_shared_from_this() {
			return std::dynamic_pointer_cast<Down>(VirtualEnableSharedFromThis::shared_from_this());
		}
	};

}
