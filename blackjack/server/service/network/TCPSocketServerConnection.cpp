#include "TCPSocketServerConnection.h"

namespace service {

	int set_nonblock(SOCKET sock) {
		u_long flags = 1;
		return ioctlsocket(sock, FIONBIO, &flags);
	}


	ConnectionId generate_id_from_addr(const sockaddr_in& addr) {
		ConnectionId id(inet_ntoa(addr.sin_addr));
		id += ":" + std::to_string(addr.sin_port);
		return id;
	}



	// TCPSocketServerConnection
	TCPSocketServerConnection::TCPSocketServerConnection()
		: masterSocket_(INVALID_SOCKET)
		, port_(DefaultPort) {
	}


	TCPSocketServerConnection::TCPSocketServerConnection(int port)
		: masterSocket_(INVALID_SOCKET)
		, port_(port) {
	}


	TCPSocketServerConnection::~TCPSocketServerConnection() {         
		shutdown(masterSocket_, SD_BOTH);
		closesocket(masterSocket_);
	}


	SOCKET TCPSocketServerConnection::find(ConnectionId id) {
		for (auto c : connections_) {
			if (c.second == id) {
				return c.first;
			}
		}
		return INVALID_SOCKET;
	}


	void TCPSocketServerConnection::close(SOCKET sock, bool shdwn) {
		if (shdwn) {
			shutdown(sock, SD_BOTH);
			closesocket(sock);
		}

		slaveSockets_.erase(sock);

		if (connections_.count(sock)) {
			ConnectionId id = connections_[sock];
			connections_.erase(sock);
			if (nullptr != onClose_) {
				onClose_(id);
			}
		}
	}


	bool TCPSocketServerConnection::initialize() {

		// Initialize Winsock
		WSADATA wsaData;
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			if (nullptr != onThrow_) {
				onThrow_("WSAStartup failed with error: " + std::to_string(iResult));
			}
			return false;
		}

		// Create master socket  
		masterSocket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		sockaddr_in saddr;
		saddr.sin_family = AF_INET;
		saddr.sin_port = htons(port_);
		inet_pton(AF_INET, "0.0.0.0", &(saddr.sin_addr));

		// Setup the TCP listening socket             
		if (bind(masterSocket_, (sockaddr*)&saddr, sizeof(saddr))) {
			if (nullptr != onThrow_) {
				onThrow_("Socket bind failed with error: " + std::to_string(WSAGetLastError()));
			}
			WSACleanup();
			return false;
		}

		set_nonblock(masterSocket_);
		if (listen(masterSocket_, SOMAXCONN)) {
			if (nullptr != onThrow_) {
				onThrow_("Socket listen failed with error: " + std::to_string(WSAGetLastError()));
			}
			WSACleanup();
			return false;
		}

		int optval = 1;
		setsockopt(masterSocket_, SOL_SOCKET, SO_REUSEADDR, (const char *)&optval, sizeof(optval));
		return true;
	}


	bool TCPSocketServerConnection::handleConnectionActivity() {
		pollfd mpfd[2048];
		memset(mpfd, 0, sizeof(mpfd));

		// Add master socket
		mpfd[0].fd = masterSocket_;
		mpfd[0].events = POLLIN;

		// Add slaves
		int k = 1;
		for (auto it = slaveSockets_.begin(); it != slaveSockets_.end(); ++it) {
			mpfd[k].fd = *it;
			mpfd[k++].events = POLLIN;
		}

		// Sleep before the first activity
		WSAPoll(mpfd, (ULONG)slaveSockets_.size() + 1, -1);

		if (mpfd[0].revents & POLLIN) {

			// Accept a client socket
			sockaddr_in saddr_client;
			socklen_t size_saddr_client = sizeof(saddr_client);
			SOCKET s_socket = accept(masterSocket_, (sockaddr*)&saddr_client, &size_saddr_client);

			set_nonblock(s_socket);
			slaveSockets_.insert(s_socket);

			ConnectionId id = generate_id_from_addr(saddr_client);
			connections_[s_socket] = id;
			if (nullptr != onConnect_) {
				onConnect_(id);
			}
		}


		std::vector<SOCKET> v_to_del;

		for (int i = 1, l = (int)slaveSockets_.size() + 1; i < l; ++i) {
			SOCKET fd = mpfd[i].fd;

			// Read message
			if (mpfd[i].revents & POLLIN) {
				char buf[1024] = { 0 };

				int cnt = recv(fd, buf, 1023, 0);
				if (cnt == 0 && errno != EAGAIN) { 
					shutdown(fd, SD_BOTH);
					closesocket(fd);
					v_to_del.push_back(fd);
				}

				else if (cnt > 0) {
					buf[1023] = '\0';
					if (nullptr != onRead_) {
						onRead_(connections_[fd], std::string(buf));
					}
				}
			}

			// If client is disconnected
			if (mpfd[i].revents & POLLHUP && errno != EAGAIN) {
				std::cout << "Error: " << errno;
				shutdown(fd, SD_BOTH);
				closesocket(fd);
				v_to_del.push_back(fd);
			}
		}

		for (auto it = v_to_del.begin(); it != v_to_del.end(); ++it) {
			close(*it, false);
		}

		return true;
	}


	bool TCPSocketServerConnection::write(ConnectionId to, std::string text) {
		SOCKET sock = find(to);
		if (INVALID_SOCKET == sock) {
			if (nullptr != onThrow_) {
				onThrow_("Connection " + to + " is not found");
			}
			return false;
		}

		int result = send(sock, text.c_str(), (int)text.size(), 0);
		if (SOCKET_ERROR == result) {
			if (nullptr != onThrow_) {
				onThrow_("Send failed with error : " + std::to_string(WSAGetLastError()));
			}
			close(sock);
			return false;
		}
		return true;
	}


	void TCPSocketServerConnection::broadcast(ConnectionId except, std::string text) {
		for (auto e : connections_) {
			if (e.second != except) {
				write(e.second, text);
			}
		}
	}


	void TCPSocketServerConnection::disconnect(ConnectionId user) {
		SOCKET sock = find(user);
		if (INVALID_SOCKET != sock) {
			close(sock);
		}
	}
}