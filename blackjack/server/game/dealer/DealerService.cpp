#include "DealerService.h"
#include "../frontend/CommandToFrontend.h"

#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <cassert>


std::unordered_map<PlayerState, std::string> PlayerStateToString = {
	{JOIN, "JOIN"},
	{BET, "BET" },
	{START, "START" },
	{CHECK, "CHECK" },
	{THINK, "THINK" },
	{HIT, "HIT" },
	{STAND, "STAND" },
	{BLACKJACK, "BLACKJACK" },
	{TWENTYONE, "TWENTYONE" },
	{WIN, "WIN" },
	{LOSE, "LOSE" },
	{BUST , "BUST" },
	{OBSERVER, "OBSERVER" }
};


StringVec split(std::string s) {
	StringVec tokens;
	std::string token;

	std::for_each(s.begin(), s.end(), [&](char c) {
		if (!isspace(c))
			token += c;
		else {
			if (token.length()) tokens.push_back(token);
			token.clear();
		}
	});

	if (token.length())
		tokens.push_back(token);
	return tokens;
}


std::string merge(StringVec vec, int begin = 0) {
	std::string result;

	for (int i = begin; i < (int)vec.size(); i++) {
		result += vec[i];
		result += " ";
	}

	return result;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
// DealerService
const std::string DealerService::DEALER_SERVICE_NAME("DealerService");

DealerService::DealerService(int workersCount)
	: CommandService(workersCount)
	, feedback_(getAddress())
	, maxThinkTimeMsec_(60000)
	, sleepTimeMsec_(100)
	, isGameStarted_(false)
	, dealer_(nullptr) {
}


std::string DealerService::getName() {
	return DEALER_SERVICE_NAME;
}


bool DealerService::initialize() {
	dealer_ = std::make_shared<Player>(std::make_shared<UserProfile>("Dealer", 0));

	initPlayerStateHandlers();
	initPlayerCommands();

	auto sharedThis = downcasted_shared_from_this<DealerService>();
	workers_.emplace_back(new GameWorker(sharedThis));

	return CommandService::initialize();
}




void DealerService::initPlayerStateHandlers() {
	setPlayerStateHandler(JOIN, [&](PlayerPtr player) {
		setState(player, !isGameStarted_ ? BET : OBSERVER);
	});



	setPlayerStateHandler(START, [&](PlayerPtr player) {
		takeCard(player, 2);
		setState(player, CHECK);
	});



	setPlayerStateHandler(HIT, [&](PlayerPtr player) {
		takeCard(player, 1);
		setState(player, CHECK);
	});



	setPlayerStateHandler(CHECK, [&](PlayerPtr player) {
		if (checkHand(player)) {
			setState(player, THINK);
			feedback_.sendOfferCard(player);
		}
	});



	setPlayerStateHandler(BET, [&](PlayerPtr player) {
		if (isTimeOver(player)) {
			feedback_.sendTo(player, "Time for bet is running out...");
			playerCommands_["pass"].handler(player, {});
		}
	});



	setPlayerStateHandler(THINK, [&](PlayerPtr player) {
		if (isTimeOver(player)) {
			feedback_.sendTo(player, "Time for think is running out...");
			playerCommands_["pass"].handler(player, {});
		}
	});
}




void DealerService::initPlayerCommands() {

	setPlayerCommand("help", {
		"print command list",
		[&](PlayerPtr player, StringVec args) {
			std::string text("command list:\n");
			for (auto c : playerCommands_) {
				text += c.first;
				text += " - ";
				text += c.second.description;
				text += "\n";
			}
			feedback_.sendTo(player, text, true);
		}
	});



	setPlayerCommand("bet", {
		"place the bet",
		[&](PlayerPtr player, StringVec args) {
			if ((player->getState() & BET) == 0) {
				feedback_.sendTo(player, "you can't do it");
				return;
			}

			if (args.size() < 2) {
				feedback_.sendTo(player, "usage: bet <number>");
				return;
			}

			int bet = atoi(args[1].c_str());
			if (bet <= 0) {
				feedback_.sendTo(player, "incorrect bet");
				return;
			}

			setState(player, START, [&]() {
				if (player->trySetBet(bet)) {
					feedback_.sendBet(player);
					return true;
				}
				feedback_.sendTo(player, "not enough money");
				return false; 
			});
		}
	});



	setPlayerCommand("hit", {
		"take a card from dealer",
		[&](PlayerPtr player, StringVec args) {
			setState(player, HIT, [&]() {
				if ((player->getState() & PLAY) == 0) {
					feedback_.sendTo(player, "you can't do it");
					return false;
				}
				return true;
			});
		}
	});



	setPlayerCommand("stand", {
		"don't take more cards",
		[&](PlayerPtr player, StringVec args) {
			setState(player, STAND, [&]() {
				if ((player->getState() & PLAY) == 0) {
					feedback_.sendTo(player, "you can't do it");
					return false;
				}
				feedback_.sendStand(player);
				return true; 
			});
		}
	});



	setPlayerCommand("players", {
		"print players list",
		[&](PlayerPtr player, StringVec args) {
			std::string text("players list:\n");
			for (auto p : players_) {
				text += p.second->getName();
				text += "\n";
			}
			feedback_.sendTo(player, text, true);
		}
	});



	setPlayerCommand("private", {
		"send private message to player",
		[&](PlayerPtr player, StringVec args) {
			if (args.size() < 3) {
				feedback_.sendTo(player, "Usage: private <name> <text>");
				return;
			}

			std::string name = args[1];
			PlayerPtr to = getPlayerByName(name);
			if (nullptr == to) {
				feedback_.sendTo(player, name + "not found");
				return;
			}

			std::string text;
			text += player->getName();
			text += " > ";
			text += merge(args, 2);
			feedback_.sendTo(to, text);
		}
	});



	setPlayerCommand("say", {
		"send message to all players",
		[&](PlayerPtr player, StringVec args) {
			if (args.size() < 2) {
				feedback_.sendTo(player, "Usage: chat <text>");
				return;
			}

			std::string text;
			text += player->getName();
			text += " > ";
			text += merge(args, 1);
			feedback_.sendAll(player, text);
		}
	});



	setPlayerCommand("about", {
		"send info about player",
		[&](PlayerPtr player, StringVec args) {
			PlayerPtr about = player;
			if (args.size() > 2)
				about = getPlayerByName(args[1]);
			feedback_.sendInfoAbout(about, true);
		}
	});



	setPlayerCommand("hand", {
		"send player cards",
		[&](PlayerPtr player, StringVec args) {
			feedback_.sendHand(player);
		}
	});



	setPlayerCommand("pass", {
		"pass current game",
		[&](PlayerPtr player, StringVec args) {
			player->setState(OBSERVER);
			feedback_.sendPass(player);
		}
	});
}




void DealerService::setPlayerCommand(std::string name, PlayerCommand command) {
	playerCommands_[name] = command;
}


void DealerService::setPlayerStateHandler(PlayerState state, PlayerStateHandler handler) {
	playerStateHandlers_[state] = handler;
}



// game phases. If return true - we can go to the next
bool DealerService::startGamePhase() {
	// if game already started
	if (isGameStarted_) return true;

	// drop hands & resuffle deck
	deck_.shuffle();
	dropHand(dealer_);
	for (auto p : players_) {
		dropHand(p.second);
	}

	// request bets
	setState(BET, [&](PlayerPtr p) {
		if (p->getUser()->getCash() <= 0) {
			feedback_.sendTo(p, "you have no money, go away!");
			CommandSystem::send(new CommandDisconnect(getAddress(), p->getUser()));
			return false;
		}
		return true;
	});

	// if players have no money
	if (getPlayersByStateCount(BET) == 0)
		return false;

	isGameStarted_ = true;
	feedback_.sendStartGame();
	return true;
}


bool DealerService::placeBetsPhase() {
	// all players must place bet
	PlayersVec players = getPlayersByState(BET);
	if (!players.empty()) {
		handlePlayersState(players);
		return false;
	}
	return true;
}


bool DealerService::playersGamePhase() {
	// players take cards
	handlePlayersState(getPlayersByState(PLAY));
	return getPlayersByStateCount(PLAY) == 0;
}


bool DealerService::dealerGamePhase() {
	PlayersVec players = getPlayersByState(WAITDEALERGAME);
	if (players.empty()) return true;

	feedback_.sendAll(nullptr, "\nDealer take cards...");

	int dealerScore;
	deck::Hand& dealerHand = dealer_->getHand();
	while ((dealerScore = dealerHand.getScore()) < 17) {
		takeCard(dealer_);
	}
	setState(dealer_, STAND);

	if (checkBust(dealer_)) { // all players win
		changeState(WAITDEALERGAME, WIN);
		return true;
	}

	if (checkBlackjack(dealer_)) { // all players lose
		setState(LOSE);
		return true;
	}

	// dealer hasn't blackjack => players with blackjack win
	changeState(BLACKJACK, WIN);

	// dealer has 21 => other players lose
	if (checkTwentyOne(dealer_)) {
		changeState(static_cast<PlayerState>(STAND | TWENTYONE), LOSE);
		return true;
	}

	// dealer hasn't 21 => players with 21 win
	changeState(TWENTYONE, WIN);

	// dealer play with stand players
	players = getPlayersByState(STAND);
	for (auto player : players) {
		int playerrScore = player->getHand().getScore();
		if (playerrScore > dealerScore) {
			setState(player, WIN);
			continue;
		}
		setState(player, LOSE);
	}

	players = getPlayersByState(WAITDEALERGAME);
	assert(players.empty());
	changeState(WAITDEALERGAME, OBSERVER);
	return true;
}


bool DealerService::endGamePhase() {
	if (getPlayersByStateCount(static_cast<PlayerState>(PLAY | WAITDEALERGAME)) == 0) {
		PlayersVec players(getPlayersByState(INGAME));
		players.push_back(dealer_);
		feedback_.sendGameResults(players);

		isGameStarted_ = false;
		setState(JOIN);

		// dealer has a rest, drinks and reshuffles cards :)
		feedback_.sendAll(nullptr, "\n\nPreparing for a new game.......");
		std::this_thread::sleep_for(std::chrono::seconds(4));
	}

	return true;
}



// players functions
PlayerPtr DealerService::getPlayerByName(std::string name) {
	PlayerPtr player;
	for (auto p : players_) {
		if (p.second->getName() == name) {
			player = p.second;
			break;
		}
	}
	return player;
}


PlayersVec DealerService::getPlayersByState(PlayerState state) {
	PlayersVec result;
	for (auto p : players_) {
		if (p.second->getState() & state) {
			result.emplace_back(p.second);
		}
	}
	return result;
}


int DealerService::getPlayersByStateCount(PlayerState state) {
	int count = 0;
	for (auto p : players_) {
		if (p.second->getState() & state) {
			count++;
		}
	}
	return count;
}


void DealerService::setState(PlayerState state) {
	for (auto p : players_) {
		setState(p.second, state);
	}
}


void DealerService::setState(PlayerPtr player, PlayerState state) {
	std::string text;
	text += player->getName();
	text += ": ";
	text += PlayerStateToString[player->getState()];
	text += "->";
	text += PlayerStateToString[state];
	getLogger() << text;

	player->setState(state);
}


void DealerService::setState(PlayerPtr player, PlayerState state, std::function<bool(void)> setIf) {
	if (setIf()) {
		setState(player, state);
	} 
}


void DealerService::setState(PlayerState state, std::function<bool(PlayerPtr)> setIf) {
	for (auto p : players_) {
		PlayerPtr player = p.second;
		if (setIf(player)) {
			setState(player, state);
		}
	}
}


void DealerService::changeState(PlayerState oldst, PlayerState newst) {
	for (auto e : players_) {
		changeState(e.second, oldst, newst);
	}
}


void DealerService::changeState(PlayerPtr player, PlayerState oldst, PlayerState newst) {
	if (player->getState() & oldst) {
		player->setState(newst);
	}
}


void DealerService::handlePlayerState(PlayerPtr player) {
	PlayerState state = player->getState();
	if (playerStateHandlers_.count(state)) {
		playerStateHandlers_[state](player);
	}
}


void DealerService::handlePlayersState(PlayersVec players) {
	for (auto p : players) {
		handlePlayerState(p);
	}
}



// card functions
bool DealerService::isTimeOver(PlayerPtr player) {
	return player->getThinkCounter() * sleepTimeMsec_ > maxThinkTimeMsec_;
}


void DealerService::takeCard(PlayerPtr player, int count) {
	deck::Hand& hand = player->getHand();
	for (int i = 0; i < count; i++) {
		deck::Card card = deck_.pop();
		hand.add(card);

		feedback_.sendTakenCard(player, card);
	}
}


void DealerService::dropHand(PlayerPtr player) {
	player->getHand().drop();
}


bool DealerService::checkHand(PlayerPtr player) {
	return !checkBlackjack(player) && !checkTwentyOne(player) && !checkBust(player);
}


bool DealerService::checkBlackjack(PlayerPtr player) {
	deck::Hand& hand = player->getHand();
	bool result = hand.isBlackjack();
	if (result) {
		setState(player, BLACKJACK);
	}
	return result;
}


bool DealerService::checkTwentyOne(PlayerPtr player) {
	deck::Hand& hand = player->getHand();
	bool result = hand.isTwentyOne();
	if (result) {
		setState(player, TWENTYONE);
	}
	return result;
}


bool DealerService::checkBust(PlayerPtr player) {
	bool result = player->getHand().isBust();
	if (result) {
		setState(player, BUST);
	}
	return result;
}



// dealer service functionality
bool DealerService::handleGameLoop() {
	std::this_thread::sleep_for(std::chrono::milliseconds(sleepTimeMsec_));
	std::lock_guard<std::recursive_mutex> lock(mutex_);
	
	for (;;) {
		if (players_.empty()) break;

		if (!startGamePhase()) break;
		if (!placeBetsPhase()) break;
		if (!playersGamePhase()) break;
		if (!dealerGamePhase()) break;

		endGamePhase();
		break;
	}

	return true;
}


bool DealerService::addPlayer(UserProfilePtr user) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);
	if (nullptr == user) return false;
	if (players_.count(user))
		return false;

	PlayerPtr player = std::make_shared<Player>(user);
	players_[user] = player;

	feedback_.sendWelcome(player, isGameStarted_);
	playerCommands_["help"].handler(player, {});
	playerCommands_["players"].handler(player, {});
	return true;
}


bool DealerService::removePlayer(UserProfilePtr user) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);
	if (nullptr == user) return false;
	if (!players_.count(user))
		return false;

	PlayerPtr player = players_[user];
	players_.erase(user);

	feedback_.sendGoodbye(player);
	return true;
}


bool DealerService::parsePlayerCommand(UserProfilePtr user, std::string text) {
	std::lock_guard<std::recursive_mutex> lock(mutex_);
	if (text.empty()) return false;
	if (!players_.count(user))
		return false;

	PlayerPtr player = players_[user];

	StringVec tokens(split(text));
	if (playerCommands_.count(tokens[0])) {
		playerCommands_[tokens[0]].handler(player, tokens);
		return true;
	}

	tokens.insert(tokens.begin(), "say");
	playerCommands_["say"].handler(player, tokens);
	return true;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// GameWorker
DealerService::GameWorker::GameWorker(DealerServicePtr service)
	: service(service) {
}

std::string DealerService::GameWorker::getName() {
	return "GameWorker";
}

bool DealerService::GameWorker::doWork() {
	if (DealerServicePtr servLock = service.lock()) {
		return servLock->handleGameLoop();
	}
	return false;
}

