#pragma once
#include "DealerService.h"
#include "../account/UserProfile.h"
#include "../../service/command/Command.h"
#include "../../service/service/Service.h"


class CommandToDealer : public CommandTo<DealerService> {
public:
	CommandToDealer(Address from);
	virtual ~CommandToDealer() = default;
};



class CommandAddPlayer : public CommandToDealer {
private:
	UserProfilePtr user_;

public:
	CommandAddPlayer(Address from, UserProfilePtr user);
	virtual ~CommandAddPlayer() = default;

	virtual bool execute(DealerServicePtr service);
	virtual std::string toString() override;
};



class CommandRemovePlayer : public CommandToDealer {
private:
	UserProfilePtr user_;

public:
	CommandRemovePlayer(Address from, UserProfilePtr user);
	virtual ~CommandRemovePlayer() = default;

	virtual bool execute(DealerServicePtr service);
	virtual std::string toString() override;
};



class CommandHandleMessage : public CommandToDealer {
private:
	UserProfilePtr user_;
	std::string text_;

public:
	CommandHandleMessage(Address from, UserProfilePtr user, std::string text);
	virtual ~CommandHandleMessage() = default;

	virtual bool execute(DealerServicePtr service);
	virtual std::string toString() override;
};